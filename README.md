# Projeto Frontend Pagamentos Pix
### Desenvolver um projeto do zero com Angular 2+

#### Desenvolver uma interface para consumir uma API RestFul.

### Preparação

• Instalar o Visual Studio Code(https://code.visualstudio.com/);

• Instalar o Git (https://gitforwindows.org/);

• Criar um repositório de preferencia no https://bitbucket.org/ para o projeto chamado de front-pagamentos-pix.git (https://www.youtube.com/watch?v=O2DFKHla80A);

•Instalar extensão Angular Language Service;

•Instalar NodeJs versão 10(https://nodejs.org/dist/latest-v10.x/node-v10.22.0-x64.msi);

•Instalar pelo git bash “npm i @angular/cli@1.7.4 --save-dev”;

•Utilizar o bootstrap https://getbootstrap.com/docs/4.5/getting-started/download/;

•Obs: a estilização do projeto é por sua conta, as imagens apresentadas neste documento são apenas para servir de guia.